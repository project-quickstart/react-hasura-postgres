#!/bin/bash

cd hasura/

socat TCP-LISTEN:8080,fork TCP:hasura:8080 & {
    
    hasura migrate apply --database-name default || exit 1

    hasura metadata apply || exit 1
}


