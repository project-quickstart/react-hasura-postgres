# syntax=docker/dockerfile:1.4

FROM node:lts AS development

ENV CI=true
ENV PORT=3000

WORKDIR /workspace
COPY package.json .
COPY package-lock.json .
RUN npm ci
COPY . .


FROM development AS builder

RUN npm run build

# Environnement de dev
FROM development as dev-envs

# Installation de git
RUN apt-get update && apt-get install -y --no-install-recommends git && apt-get install -y socat

# Installation de la CLI d'Hasura
RUN curl -L https://github.com/hasura/graphql-engine/raw/stable/cli/get.sh | bash

RUN useradd -s /bin/bash -m vscode && groupadd docker && usermod -aG docker vscode

# install Docker tools (cli, buildx, compose)
COPY --from=gloursdocker/docker / /

CMD [ "/bin/sh", "-c", "'while sleep 1000; do:; done'" ]

FROM nginx:1.13-alpine

COPY --from=builder /workspace/build /usr/share/nginx/html