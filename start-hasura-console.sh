#!/bin/bash

cd /workspace/hasura

socat TCP-LISTEN:8080,fork TCP:hasura:8080 & hasura console --console-port 8081