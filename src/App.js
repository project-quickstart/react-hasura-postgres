import logo from './logo.svg';
import './App.css';

// Import everything needed to use the `useQuery` hook
import { useQuery, gql } from '@apollo/client';

const GET_HELLO_WORLD = gql`
  query GetHelloWorld {
    hello_world(limit: 1) {
      name
    }
  }
`;

function DisplayHelloWorld() {
  const { loading, error, data } = useQuery(GET_HELLO_WORLD);

  if (loading) return <p>Loading...</p>;
  if (error) return <p>Error : {error.message}</p>;

  return data.hello_world.map(({ name }) => (
    <div>
      <h3>{name}</h3>
    </div>
  ));
}

export default function App() {
  return (
    <div>
      <h2>My first Apollo app 🚀</h2>
      <DisplayHelloWorld />
    </div>
  );
}
