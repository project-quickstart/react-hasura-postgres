# Hasura

## Migrations automatiques

Au lancement du conteneur de développement, le script `.devcontainer/postCreate.sh` va automatiquement lier Hasura à la base de données Postgres, et lancer les migrations présentes dans le projet.

Vous pouvez commenter ces lignes afin de désactiver ce comportement.

## Lancer la console

La console Hasura permet de modifier de façon interactive et visuelle le schéma de données de votre application.

Pour pouvoir l'exécuter, un petit script est disponible à la racine du template, que vous pouvez exécuter comme suit :

`./start-hasura-console.sh`

## Squash plusieurs migrations

Chaque modification dans la console peut créer des fichiers de migrations. Au bout d'un moment,
ça peut faire beaucoup de fichiers.

Il y a la possibilité de _squash_ plusieurs migrations pour n'en former qu'une. C'est 
particulièrement utile quand vous développez une fonctionnalité et que vous voulez
squash toutes les migrations qui touchent à cette fonctionnalité.

Pour cela, tapez simplement la commande suivante :

`hasura migrate squash --name "<ma-fonctionnalité>" --from <timestamp-de-la-première-migration-à-squash> --database-name postgres`

Le timestamp est simplement le chiffre du début du nom de dossier contenant la migration.

Une fois le squash effectué, il faut marquer le squash comme ayant déjà été appliqué :

`hasura migrate apply --version "<timestamp-du-squash>" --skip-execution --database-name postgres`