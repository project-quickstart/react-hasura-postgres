# Installation

## Pré-requis

Pour fonctionner, vous n'aurez besoin que de deux choses :

- Visual Studio Code
- L'extension VSCode "Dev Containers"
- Docker Desktop

## Premier lancement du conteneur

1. Ouvrez VSCode
2. Appuyez sur F1, et tapez `Dev Containers: Clone Repository in Container Volume...`
3. Donnez l'URL de ce projet git, c'est à dire `https://gitlab.com/project-quickstart/react-hasura-postgres`
4. Patientez quelques instants.

Une fois le conteneur lancé, tout devrait fonctionner automatiquement. 

Pour vérifier que tout s'est bien passé, ouvrez votre navigateur à l'adresse <http://localhost:3000/>
Vous devriez voir l'application React tourner sans erreur !

## Adapter le template à son projet

Dans un premier temps, assurez vous d'avoir un projet git vide créé sur Gitlab ou GitHub.
Depuis le conteneur, vous pouvez facilement remplacer l'url de la remote git `origin` pour pointer vers votre projet.

N'hésitez également pas à changer toutes les instances du nom `react-hasura-postgres` par votre nom de projet dans les fichiers suivants :

- `package.json`
- `.devContainer/devcontainer.json`

Poussez vos changements sur votre dépôt git, et reprenez les étapes de la section "Premier lancement du conteneur" avec cette fois l'URL de votre propre projet git.
