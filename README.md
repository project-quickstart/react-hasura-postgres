# React-hasura-postegres template

Projet vierge utilisant React, Hasura et Postgres, avec un environnement de développement se basant sur les conteneurs de développement de Visual Studio Code.

## Description

J'ai créé ce projet pour m'entraîner encore un peu plus à utiliser Docker. Mon objectif était de créer un template de projet vide prêt à l'emploi se basant sur React, Hasura et Postgres. La contrainte que je voulais me fixer était que l'environnement de développement devait se baser sur Docker, d'où le choix des VSCode Dev Containers.

## Utilisation

Vous voulez lancer rapidement un projet avec cette configuration ? [Suivez le guide !](docs/INSTALLATION.md)

## Gestion des données

Vous voulez voir comment utiliser Hasura et les migrations de données ? [Un guide est également disponible !](docs/HASURA.md)
